# Learning

Here I track my learning goals and progress

Part I of this text list my motivations for learning
Part II are the skills that I currently lack and will be working on

Part I : My long term goals are:

A) Updating old android devices with the latest patches:
Maintaining the numerous devices I own which the original manufacturers 
have stopped supporting.

My primary motivation was cost, having to buy a new device once every year or 
two simply because the older one was no longer supported by applications.
In contrast my earlier devices (e.g. s30, s40 nokia devices) would last three 
years atleast untill they would physically break and needed to be replaced.

Other motivations include:
-Environmental harm: Its wasteful to produce such a large number of new devices.
Safely disposing off electronic waste is also a big issue. Currently they are
dumped to third world countries under the guise of charity.

-Violation of our rights and freedoms: Including the right to repair the
device. Something as simple as replacing the battery is not allowed by the user
and violates warranties for an increasing number of devices.
The right to see and change the source code of our device drivers and other 
closed source components. This is a very basic right as a consumer to be able to
view and modify what is actually running on the devices that we have paid for.

B) Datascience and machine learning:
 
The mathematicals tools of probability and statistics were present well before
the modern computer arrived. The machines that could process large amounts of 
data and the researchers themselves were few. Technology advanced, computing 
became cheaper and the number of people who started taking up research increased.

Today thanks to everyone who believed that knowledge must be shared we have
tools and learning platforms where anyone with the will and an internet 
connection can learn atleast the basics of research.

I hope to learn datascience so that I can aid in medical research. I haven't 
decided what particular problem area I wish to pursue. Some candidates are:
-aging and longevity
-cure for cancer and other dieases
-pharma research: how to make cheaper, more effective and more accessible drugs

C) Space exploration:
"Born too late to explore the world. Born too early to explore the galaxy." ~anon
Shows like cosmos the original by Carl Sagan,
movies like Lost in space, Star Trek, StarWars, Aliens, 
plus shows on discovery channel, video game adaptations of the above, 
sci-fi anime etc. Those were the things that introduced me to space "the final
frontier". The thrill of exploration fills me immediately and I go into another 
world for some time.

We don't yet have the means to explore space but people far smarter and dedicated
than I'll ever be are working on it. I atleast hope to learn and if possible 
contribute in some way.

"If you want to build a ship, don’t drum up the men to gather wood, 
divide the work, and give orders. Instead, teach them to yearn for the 
vast and endless sea."
― Antoine de Saint-Exupéry


After we kissed the sun
We can burn our names into the sky
...
Diamond clouds
I think we're getting closer
Don't stop now
Off the ground
Higher than a supernova
Don't look down
Beam me up!
~ "Beam me up" by Cazzette

And for you my dear reader

"In the vastness of space and the immensity of time,
it is my joy to share a planet and an epoch with Annie."
― Carl Sagan
[Dedication to Sagan's wife, Ann Druyan, in Cosmos]


D) AI:
I believe AI is the future of the human race. Increasingly intelligent beings
that are able to shape the decisions we take. Humans aren't inherently rational
animals so I am hoping we are able to create beings better than ourselves.


Part II:

That was s a long list and at the back of it all are computers and maths
Below is list of skills I hope to learn.
I must admit I don't have a proper plan and I'm just winging it.

A) Computer Languages:
C - for any performance related stuff like OS kernels whether for mobile devices
or spaceships or high performance computing for research. Others: C++

Java - Not so performant as it runs in the JVM. Write once and deploy on any 
system that support the JVM. Most popular application dev language. 
Others: Only Python comes close in app dev in terms of popularity.

Python - One of the languages used for datascience and machine learning. Thanks
to being a functional programming language that is good at text processing by 
design. It also has a large amount of third party distributions like anaconda
for data science.
Others: R

BASH Shell scripting: Start here if absolutely new to computers.

Misc: Introductory courses if you are new to application programming:
Learning application programming at https://freecodecamp.org/ 
thanks to https://www.reddit.com/user/lowkrsbhs for making me aware of this.

B) Tools and platforms:
Anaconda - A data science focused distibution of python.
GNU/Linux - The most popular UNIX like OS that serves as a base for Android and 
other embedded devices.

C) Maths:

My resources for each of these, but a simple textbook should suffice.

1) Introduction to pre-college maths :
Start here if you do not know math - algebra, geometry etc.
College Algebra and Problem Solving from Arizona State University
They use an interactive system (ALEKS) to teach math which helps a lot.
https://courses.edx.org/courses/course-v1:ASUx+MAT117x+1T2016/course/

2) Precalculus:
Start here if you do not know intergration and differentiation

https://www.edx.org/course/discovery-precalculus-creative-connected-utaustinx-ut-prec-10-03x

Another introductory course from ASU 
https://www.edx.org/course/precalculus-asux-mat170x

Oh my, one more!
https://www.edx.org/course/pre-university-calculus

3) Calculus:

4) Probabilty:
Introductory course 
https://www.edx.org/course/introduction-to-probability-0

Introduction to Probability: Part II – Inference & Processes
https://www.edx.org/course/introduction-to-probability-part-2-inference-processes

One more introductory course to really build on that strong conceptual base :)
https://www.edx.org/course/fat-chance-probability-from-the-ground-up

5) Statistics:

Foundations of Data Analysis - Part 1: Statistics Using R
https://www.edx.org/course/foundations-data-analysis-part-1-utaustinx-ut-7-11x-0

Foundations of Data Analysis - Part 2: Inferential Statistics
https://www.edx.org/course/foundations-data-analysis-part-2-utaustinx-ut-7-21x-0

D) Data science:
Start here if you already are familiar and confident about mathematical concepts

1) Python for data science:
This introduces data science related stuff for python like pandas, anaconda, numpy
https://courses.edx.org/courses/course-v1:UCSanDiegoX+DSE200x+3T2018/course/

2) Probabilty and Statistics:
How to use python to calculate probability and stats
https://www.edx.org/course/probability-and-statistics-in-data-science-using-python

3) Machine learning fundamentals:
https://www.edx.org/course/machine-learning-fundamentals

4) BigData Analytics using Spark:
https://www.edx.org/course/big-data-analytics-using-spark-uc-san-diegox-dse230x
