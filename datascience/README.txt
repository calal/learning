Here I explain my motivations for learning maths and science.


A) Importance of a science education in todays world.

Computers and automation have already made a lot of low skill and semi-skilled jobs redundant.
Tech companies like Google, Amazon, Microsoft are working towards automation and AI that will possibly make a 
lot more of current jobs irrelevant. e.g. self driving cars by Google/ Tesla, low level legal advisors by IBM Watson etc.

The technologies of the future include advancements in alternative and clean energy sources, medicene, protection of the environment, space exloration, agriculture etc.
All of those fields depend on firstly research and development plus a workforce highly skilled in STEM to support both complex hardware and software.

More and more people are completeing PHDs and getting into research. People who do not keep themselves relevant are at a risk of either getting stuck in low paying jobs or ending up jobless.

B) Data science

A data scientist is someone who is able to look at data to solve a known problem or to find areas of optimization.

Why I feel I must pursue data science as a career

I started my career and currently am a data warehouse developer. Its my job to get data from various systems and then develop and run reports on them. I started with ETL tools like informatica and the popular RDBMs and currently work on hadoop. The primary motivation of organizations to move to the latest technology is always cost. Hadoop took away licensing costs and the need for expensive hardware. The next promise of cost saving is moving data to the cloud.
In most of the migrations I have witnessed there has been little to no majog issues in adopting newer technologies. This also means that the level of skill required to be able support these kinds of applications is comparatively low.

Another aspect to data warehouses or big data systems is the possibility of querying such systems to gain insights.
Now organisations will generally hire some data sientists to overlook this effort and also train inhouse employees with the knowledge necessary to aid in the effort of maintaining the systems.

Thus I feel that for anyone currently working in the fields of data warehouseing, SQL/ RDBMs development knowledge of data science is essential if they wish to remain relevant.

C) Knowledge required for a technologist working to wards a data science career

1) Brush up on college level math focusing on probability and statistics.
2) Learn the programing languages used for data science, python and R
3) Theres a lot of free and open source data science projects go through them and make your own implementations
4) Learn atleast one enterprise programing language


MOOCs
Many available, don't just stick to one.
Examples:

Maths:
https://www.edx.org/course/precalculus-asux-mat170x
Started at 55% via pre test. 30 out of 150 new topics completed.

https://www.edx.org/course/python-for-data-science-0 - Primarily teaches usage of tools packaged in anaconda (a distribution of python) like jupyter, pandas, numpy 
https://www.edx.org/course/probability-and-statistics-in-data-science-using-python
https://www.edx.org/course/machine-learning-fundamentals

