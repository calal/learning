#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 2019

@author: gitlab.com/calal

Python version of 
https://lagunita.stanford.edu/courses/HumanitiesSciences/StatLearning/Winter2016/
Ch2 Overview of Statistical Learning > 2.R Introduction to R 

"""


import numpy as np


#%%
#create a vector of three elements
x=np.array((2,7,5))
print(type(x),x)

#%%
#create vector from sequences
    
y=np.arange(4,12,3)
print(type(y),y)

#%%
#vector addition, division, and power
print(x+y)
print(x/y)
print(x**y)

#%%
#accessing elements of vectors
print(x[1])
print(x[1:3])
#no equivalent of Rs x[-2]. i.e. return all elements except 2nd

#%%
# matrices: see also the zip method of creating matrices

A=np.arange(1,13,1).reshape(4,3)
print('Array:',A)
print('subset:',A[2:4,1:3]) 
print('first column',type(A[0:4,0]),A[0:4,0])
print('dimensions',A.shape)

#%%
#random functions
print(np.__version__)
x=np.random.uniform(0,1,50)
print(x)
y=np.random.standard_normal(50)
print(y)

#%%
#plots
import matplotlib.pyplot as plt

#plt.plot(x,y)
#plt.plot(y)
#plt.hist(x)
plt.hist(y)

#%%
#reading csv files

import pandas as pd

file=pd.read_csv("./abc.csv")

print(file.head())
print(file.dtypes) # columns and data types
print(file.describe()) #summary in R
print(file[0:3])